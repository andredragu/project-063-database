﻿using UnityEngine;
using System.Collections;

public class ConnectThroughJavaScript : MonoBehaviour {

	string fileName = "partituur";
	string filePathName = "test";
	
	void Start () {
		//ABOUT Javascript reageert op de drie stappen setFileName, setFilePathName en ten slotte submitform, in pdf_server.php wordt nog een mogelijk tussenpad ingevoegd
		Application.ExternalCall("setFileName", fileName);
		Application.ExternalCall("setFilePathName", filePathName);
		Application.ExternalCall("submitform");
	}
}
